import 'dart:io';
import 'dart:math';
import 'dart:core';
import 'Dragon.dart';
import 'Goblins.dart';
import 'Player.dart';
import 'Spider.dart';
import 'Troll.dart';

const chars = "1234";

String RandomString(int strlen) {
  Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
  String result = "";
  for (var i = 0; i < strlen; i++) {
    result += chars[rnd.nextInt(chars.length)];
  }
  return result;
}

var random = RandomString(1);
void main(List<String> args) {
  Player player = Player();
  Troll troll = Troll();
  Goblins goblin = Goblins();
  Spider spider = Spider();
  Dragon dargon = Dragon();

  Opponentmenu(player, goblin, spider, troll, dargon);
}

void Opponentmenu(
    Player player, Goblins goblin, Spider spider, Troll troll, Dragon dargon) {
  opponentlist();
  var choiceselect = stdin.readLineSync()!;
  if (choiceselect == "1") {
    Goblinbattle(player, goblin);
  } else if (choiceselect == "2") {
    Spiderbattle(player, spider);
  } else if (choiceselect == "3") {
    Trollbattle(player, troll);
  } else if (choiceselect == "4") {
    Dragonbattle(player, dargon);
  } else {
    print("|--------|");
    print("|Farewell|");
    print("|--------|");
  }
}

void Dragonbattle(Player player, Dragon dargon) {
  while (player.hp > 0 || dargon.hp > 0) {
    const chars = "12345";

    String RandomString(int strlen) {
      Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
      String result = "";
      for (var i = 0; i < strlen; i++) {
        result += chars[rnd.nextInt(chars.length)];
      }
      return result;
    }
    var random = RandomString(1);
    playerskillmenu();
    var choice = stdin.readLineSync()!;
    int slash = player.skill() + player.atk;
    int doubleslash = slash+slash;
    if (choice == "1") {
      print("⣶⣖⣒⣲⠶⠶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶" +
          "\n" +
          "⣿⣿⣿⣿⣿⣷⣦⣄⡙⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠻⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠹⢿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣆⢀⠈⢻⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⢀⢀⢻⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢀⢀⢀⢿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⢀⢀⢀⣸" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣭⣭⣀⣀⣀⣠⣴⣿" +
          "\n");
      print("Dragon get attacked by Player Slash $slash Damage");
      print("\n");
      var sl = dargon.hp - slash;
      print("Dragon Hp $sl");
      dargon.hp = sl;
      if (dargon.hp <= 40) {
        print("Dragon Hp has lower than 40 or equal 40");
        print("⣿⣿⣟⡻⠿⣿⣿⣿⣿⢿⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣷⣬⡙⢿⡟⣼⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⡆⡸⣿⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⢟⣾⣿⣹⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
            "\n");
        print("Dragon get attacked by Player Double Slash $doubleslash Damage");
        var doubleSl = dargon.hp - (slash + slash);
        print("Dragon Hp $doubleSl");
        dargon.hp = doubleSl;
        if (dargon.hp <= 0) {
          dargon.die();
          break;
        }
      }
    } else {
      print("--------------");
      print("     Miss     ");
      print("--------------");
    }
    dragonskillmenu();
    int roaring = dargon.skill() + dargon.atk;
    int fly = dargon.fly() + dargon.atk;
    if (random == "1") {
      print("⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⠈⠉⠛⠛⠳⢦⡀" +
          "\n" +
          "⢀⠰⠄⢀⢀⢀⢠⣀⣀⣀⣀⣀⢀⢀⠄⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀" +
          "\n" +
          "⢀⣴⡶⢶⣤⡄⠘⠧⡄⢀⢀⢀⢀⢀⢀⢀⢀⢀⣤⣄⣀⣠⡄⠰⣦⡄⢀⠈⠉⣿⢀⢀⣀⣀⢀⢤⡄⢀⢀⣀⡀⢀⠰⢀⢀⢀⢀⣀⣤⡖" +
          "\n" +
          "⢀⢀⢀⢀⠈⠻⣤⣀⠈⠳⣦⠤⠶⠛⠛⠓⢀⢀⠙⠛⠋⠁⢀⢀⡄⢀⡀⢀⣴⣿⣄⡈⠉⢀⢀⢸⣦⠤⣤⣀⡀⡀⢀⢀⣠⣚⣉⣉⡉⢀⣀⡀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⠉⠛⢶⣤⡉⠛⢶⣾⣿⣷⣀⣀⢀⣀⡀⢀⢀⠁⢀⠙⠂⢀⠙⠿⣿⢷⣤⡀⢀⢲⡀⢀⢀⢀⣀⣀⣠⣴⣉⣉⣭⡴⠶⠆⢀⢀⠁⢀⠒" +
          "\n" +
          "⢀⠊⠉⢷⡤⠂⢀⠡⢀⢀⠙⠿⣶⣜⢛⡛⢻⣿⣿⣿⣎⣛⡀⢀⢀⢀⢀⢀⣶⣦⣤⣈⠓⠉⠙⠲⠤⠙⠿⣿⣿⣿⣿⣿⣿⣿⡶⠙⠛⠁⠂⠰⢶⣾⠷⢀⣀" +
          "\n" +
          "⢀⢀⢀⠘⢀⡶⢀⢀⢀⢀⢤⢀⠽⢿⣷⣬⣛⣯⣿⣿⡧⠉⠉⠐⠠⢀⢀⡀⢀⢀⢀⢀⢀⣠⣉⣙⢠⣀⡀⠉⠛⢟⡛⠛⣛⣉⣳⠠⠖⠒⣶⣿⣿⣶⠶⠶⠿" +
          "\n" +
          "⢀⢀⢀⢀⠈⣿⣤⣤⣄⣀⣾⡇⢀⢈⠛⠿⣿⣮⣿⣭⣷⣶⣿⣳⣄⡀⢀⠻⠶⠄⢀⠘⠻⠋⠉⣳⣮⣿⣿⢿⣶⣤⣙⠻⣯⣽⣿⣏⣉⣉⣻⣍⣉⣉⣀⣤⣤" +
          "\n" +
          "⢀⢀⢀⢀⢀⠈⢉⠙⣿⣿⣿⣧⢀⢰⠃⢀⠈⣿⡿⠿⣿⣿⣿⣦⣭⣅⡀⢀⢠⣶⣤⣀⢀⠠⢤⣉⠑⠺⠿⠠⡗⠙⢿⣿⣿⣦⡤⠤⠽⡟⠉⠉⠉⠛⠻⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢰⠏⠰⡏⠈⠁⢀⢀⠸⠆⢀⣾⣿⣇⣰⣟⢀⠙⠛⣿⣿⣿⣧⣼⣿⣿⣿⣿⣿⣶⣦⣭⣝⣛⣿⣷⣶⣤⢉⣿⣿⣿⡶⠶⠶⠉⣉⡉⡉⠛⠋" +
          "\n" +
          "⢀⢀⢀⢀⢀⠈⢀⢀⢀⢀⢀⠂⢰⣴⣀⣸⠛⠁⢸⣿⣿⡄⢀⡀⢹⣿⣿⣿⣿⠛⣿⣿⠉⢛⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠒⢀⢀⠁⢀⢀⢀⠄⣀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⠘⠁⠉⢀⢀⢀⢺⣿⣿⣷⡀⢺⣾⣿⣿⣿⣿⣦⣼⣿⣦⠈⢿⣿⣿⣿⡉⢻⡿⠿⠿⣿⣿⣿⣿⣷⣦⣤⣿⣷⡖⢀⠙⠓⠛" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⠈⠛⠉⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣿⣿⠻⣌⠁⣀⠻⡀⢀⠈⠛⢿⣿⣿⣿⣿⣿⣿⣿⣶⣤⣀⣤" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⠈⠉⠉⠉⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡸⣤⣿⡄⠉⢀⢀⢀⢀⠈⠙⢿⣿⣿⣿⣿⡿⠿⠋⢉" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⠘⢀⢈⣹⣿⣿⣿⣿⣿⣿⡉⢇⢸⣿⣷⢀⢰⢀⢀⢀⢀⢀⢀⠚⠛⠉⣀⣠⣴⣶⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣇⢀⣿⣿⣿⣿⣿⣿⣿⣿⣶⡍⢻⣿⡄⠘⠃⢀⢀⢀⢀⣠⠾⠿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢸⣿⣿⣿⡿⢿⠛⢻⣿⣿⣿⣧⣈⢿⠃⠈⢀⢀⢀⣴⡟⢁⢀⠐⠛⣻⣿⣿⣿⠿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢸⣿⣿⠋⢠⡏⢀⣾⣦⣿⣿⣿⣧⡄⢀⢀⣤⠴⢀⣁⣤⣴⠶⢒⡻⠏⠹" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢸⣿⣿⢀⢸⠃⢸⣿⣿⣿⣿⡏⢹⠃⣴⣿⣷⣶⠟⠛⢉⢀⢐⡋" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢸⣿⣿⣷⣿⡀⣿⣿⣿⣿⣿⣇⣰⣿⣿⠛⡉⣩⣴⣾⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢸⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⢋⣽⢀⢈⣹⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀⢀⣼⡟⢻⣿⣿⣿⣿⣿⣿⣟⣴⡿⠃⣴⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣴⣾⡿⢫⣾⣿⢀⣾⡟⣿⣿⣿⣿⣿⡿⢋⣠⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⠰⣿⡿⠋⢀⣾⣿⠋⣸⣿⣿⣿⣿⠿⠿⠿⢷⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣠⣿⣿⠃⢠⣿⣿⡷⠆⢀⢀⣀⣀⣴⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣠⣾⣿⣿⣧⣤⣿⣿⡿⠃⢀⣠⣽⡿⢋⣼⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⡼⣅⣤⠼⠛⠛⢿⣿⣿⣿⡿⠋⢤⠆⠈⢉⣁⣴⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣠⢀⣿⡇⢹⣿⡀⢀⣤⡾⠿⢿⣭⡄⢀⠁⢐⣾⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣤⣷⠲⣾⣿⣧⡜⠋⣉⣠⣄⡀⠈⠉⠁⣠⣶⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢧⣀⣙⠿⣷⡿⣟⣭⣷⣿⣿⣧⣭⣷⣶⣶" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣸⣷⣾⣿⡿⢟⣿⣾⣿⣟⣁" +
          "\n" +
          "⢀⠈⢀⣈⣳⣤⡀⣠⣷⣤⣽⡿⢋⣡⣶⣿⣿" +
          "\n" +
          "⢀⣤⣤⣤⣤⣠⣭⣍⣩⣭⣷⣾⣿⣿⣿" +
          "\n");
      print("Player get attacked by Dragon roaring $roaring Damage");
      print("\n");

      var ro = player.hp - roaring;
      print("Player Hp $ro");
      player.hp = ro;
      if (player.hp <= 0) {
        player.die();
        break;
      }
    } else if (random == "2") {
      print("⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢠⣾⣿⣿⠛⣿⣿⡿⠿⠛⢋⣽⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣰⣿⣿⣿⠇⢀⢀⢀⢀⣠⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⡠⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣼⣿⣿⣿⠏⢀⢀⣀⣴⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢠⡾⡇⢀⢀⢀⢀⢀⢀⢀⢀⣠⣾⣿⣿⡿⢃⣠⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⣴⣿⣿⡇⢀⢀⢀⢀⢀⢀⢀⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢠⣾⠿⣿⣿⣧⢀⢀⢀⢀⣠⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⢀⣰⣿⠏⢀⣿⣿⣿⣷⣶⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⣰⣿⣿⣄⣀⣈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠿⠛⠛⠉⠉⠁⢀⢀⢀⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠋⠁⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⠻⣿⣿⣿⣿⣿⠃⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢸⣿⣿⣿⣿⡿⠋⠹⢿⣿⣿⣿⡃⢀⠈⠙⠛⠿⢿⣆⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⠈⢿⣿⣿⠋⢀⢀⢀⢀⠙⠻⠿⡇⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢀⢀⢀⠈⠻⠃⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣰⣿⣿⣿⣿⣿⣿⠟⠋⠉⠁⢀⢀⠈⠉⠻⣿" +
          "\n" +
          "⡀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣼⣿⣿⣿⣿⡿⠋⠁⢀⢀⢀⢀⢀⢀⢀⢀⢀⢸" +
          "\n" +
          "⣿⣿⣶⣄⡀⢀⢀⢀⢀⢀⢀⢀⣀⣠⣤⣶⡾⠋⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀⣀⣤⣾⣿⣿⣿⣿⣿⠏⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣾" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣶⣶⣶⣶⣿⣿⣿⣿⡿⠋⢀⢀⢀⢀⢀⢀⢀⠈⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠇⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣾⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠋⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⡀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣴⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠁⢀⢀⢀⢀⢀⢀⠠⢴⣶⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣤⢀⢀⢀⢀⢀⢀⣠⣴⣿⣿⣿⣿⣿" +
          "\n");
      print("Dragon use dragon_breathing to player $fly Damage");
      print("\n");
      var db = player.hp - fly;
      print("Player Hp $db");
      player.hp = db;
      if (player.hp <= 0) {
        player.die();
        break;
      }
    } else {
      print("--------------");
      print("     Miss     ");
      print("--------------");
    }
  }
}

void Spiderbattle(Player player, Spider spider) {
  while (player.hp > 0 || spider.hp > 0) {
    const chars = "11223";

    String RandomString(int strlen) {
      Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
      String result = "";
      for (var i = 0; i < strlen; i++) {
        result += chars[rnd.nextInt(chars.length)];
      }
      return result;
    }

    var random = RandomString(1);
    playerskillmenu();
    var choice = stdin.readLineSync()!;
    int slash = player.skill() + player.atk;
    int doubleslash = slash+slash;
    if (choice == "1") {
      print("⣶⣖⣒⣲⠶⠶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶" +
          "\n" +
          "⣿⣿⣿⣿⣿⣷⣦⣄⡙⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠻⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠹⢿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣆⢀⠈⢻⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⢀⢀⢻⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢀⢀⢀⢿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⢀⢀⢀⣸" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣭⣭⣀⣀⣀⣠⣴⣿" +
          "\n");
      print("Spider get attacked by Player Slash $slash Damage");
      print("\n");
      var sl = spider.hp - slash;
      print("Spider Hp $sl");
      spider.hp = sl;
      if (spider.hp <= 40) {
        print("Spider Hp has lower than 40 or equal 40");
        print("⣿⣿⣟⡻⠿⣿⣿⣿⣿⢿⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣷⣬⡙⢿⡟⣼⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⡆⡸⣿⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⢟⣾⣿⣹⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
            "\n");
        print("Spider get attacked by Player Double Slash $doubleslash Damage");
        var doubleSl = spider.hp - (slash + slash);
        print("Spider Hp $doubleSl");
        spider.hp = doubleSl;
        if (spider.hp <= 0) {
          spider.die();
          break;
        }
      }
    } else {
      print("--------------");
      print("     Miss     ");
      print("--------------");
    }
    spiderskillmenu();

    int bite = spider.skill() + spider.atk;
    int wed = spider.spider_wed() + spider.atk;
    if (random == "1") {
      print("⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀⣠⣤⣴⣶⣶⣶⣶⣶⣶⣦⣤⣀⣀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⡀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⣤⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄" +
          "\n" +
          "⢀⢀⢀⢀⢀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣟⠋⠛⠉⠉⠙⣿⣿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣆" +
          "\n" +
          "⢀⢀⢀⢠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⣤⡀⣿⡇⢸⣿⡟⣻⣿⣿⣿⣿⣿⣿⣿⣿⣷⡀" +
          "\n" +
          "⢀⢀⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇⣼⡿⢠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄" +
          "\n" +
          "⢀⢠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡀" +
          "\n" +
          "⢀⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣼⣿⣿⣿⣿⡿⢻⣿⣿⣿⣿⣿⣿⣿⣧" +
          "\n" +
          "⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠏⠈⠿⠱⢣⣿⣿⣿⣿⣿⣿⣿⣿⡀" +
          "\n" +
          "⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠋⢀⢀⢀⠏⣈⡿⣿⣿⣿⣿⣿⣿⣿⡇" +
          "\n" +
          "⢸⣿⣿⣿⣿⣿⣿⠙⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢁⣴⣆⢀⢀⢰⡿⣽⣿⣿⣿⣿⣿⣿⣿⡇" +
          "\n" +
          "⠸⣿⣿⣿⣿⣿⣿⣶⣿⣿⠟⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠛⣿⣆⢀⢀⠸⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⢀⢿⣿⣿⣿⣿⣿⣿⣿⣿⠰⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠋⠙⢿⣇⠈⣿⣧⢀⢀⢻⣿⣿⣿⣿⣿⣿⡏" +
          "\n" +
          "⢀⠘⣿⣿⣿⣿⣿⣿⣿⣿⣷⡄⠉⠛⠛⠿⣿⣯⠉⠻⣿⡀⢀⢀⢀⠻⢀⠘⣿⣇⣠⣾⣿⣿⣿⣿⣿⡿⠁" +
          "\n" +
          "⢀⢀⠘⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⣙⠆⣿⣿⣷⣄⡙⠃⢀⢀⢀⢀⢀⣨⣿⣿⣿⣿⣿⣿⣿⣿⡿⠁" +
          "\n" +
          "⢀⢀⢀⠘⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣭⣽⣾⣝⣿⣿⣤⣠⣤⣤⣤⣤⣾⣿⣿⣿⣿⣿⣿⣿⣿⡟⠁" +
          "\n" +
          "⢀⢀⢀⢀⢀⠹⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠋" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⠙⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠋" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⠈⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠋⠁" +
          "\n");
      print("Player get attacked by Spider bite $bite Damage");
      print("\n");

      var sc = player.hp - bite;
      print("Player Hp $sc");
      player.hp = sc;
      if (player.hp <= 0) {
        player.die();
        break;
      }
    } else if (random == "2") {
      print("⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⠛⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣸⠃⠈⢙⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⠃⢀⡀⠈⢀⢀⠉⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣦⣴⡁⢀⢀⢀⢀⢀⢀⢀⠙⠻⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣶⣤⣀⡀⢀⢀⢀⢀⢀⠈⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⣀⡀⢀⢀⢀⢀⢀⠙⠻⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣤⣄⡀⢀⢀⢀⢀⠈⠙⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣦⣄⣀⢀⢀⢀⢀⠉⠛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⣄⡀⢀⢀⢀⠈⠙⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⣀⢀⢀⢀⠉⠛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⣀⢀⢀⠈⠙⠻⣿⣿⣿⣿⣿⣿" +
          "\n");
      print("Spider use spider web to player $wed Damage");
      print("\n");
      var sd = player.hp - wed;
      print("Player Hp $sd");
      player.hp = sd;
      if (player.hp <= 0) {
        player.die();
        break;
      }
    } else {
      print("--------------");
      print("     Miss     ");
      print("--------------");
    }
  }
}

void Goblinbattle(Player player, Goblins goblin) {
  while (player.hp > 0 || goblin.hp > 0) {
    const chars = "1112";
    const charside = "123";
    String RandomString(int strlen) {
      Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
      String result = "";
      for (var i = 0; i < strlen; i++) {
        result += chars[rnd.nextInt(chars.length)];
      }
      return result;
    }
    String RandomStringSide(int strlen) {
      Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
      String result = "";
      for (var i = 0; i < strlen; i++) {
        result += charside[rnd.nextInt(charside.length)];
      }
      return result;
    }
    var random = RandomString(1);
    var randomside = RandomStringSide(1);
    playerskillmenu();
    var choice = stdin.readLineSync()!;
    int slash = player.skill() + player.atk;
    int doubleslash = slash+slash;
    var walk = goblin.walk();

    if (choice == "1") {
      walk;
      objgoblin();
      if (randomside == "1") {
        print("\n");
        print("Player side slash");
        var choicespecialplayer = stdin.readLineSync()!;
        if (choicespecialplayer == "1") {
          print("⣶⣖⣒⣲⠶⠶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶" +
              "\n" +
              "⣿⣿⣿⣿⣿⣷⣦⣄⡙⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠻⣿⣿⣿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠹⢿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣆⢀⠈⢻⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⢀⢀⢻⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢀⢀⢀⢿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⢀⢀⢀⣸" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣭⣭⣀⣀⣀⣠⣴⣿" +
              "\n");
          print("Goblin get attacked by Player Slash $slash Damage");
          print("\n");
          var sl = goblin.hp - slash;
          print("Goblin Hp $sl");
          goblin.hp = sl;
          if (goblin.hp <= 40) {
            print("Goblin Hp has lower than 40 or equal 40");
            print("⣿⣿⣟⡻⠿⣿⣿⣿⣿⢿⣿⣿⣿⣿" +
                "\n" +
                "⣿⣿⣿⣿⣷⣬⡙⢿⡟⣼⣿⣿⣿⣿" +
                "\n" +
                "⣿⣿⣿⣿⣿⣿⣿⡆⡸⣿⣿⣿⣿⣿" +
                "\n" +
                "⣿⣿⣿⣿⣿⣿⢟⣾⣿⣹⣿⣿⣿⣿" +
                "\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
                "\n");
            print("Goblin get attacked by Player Double Slash $doubleslash Damage");
            var doubleSl = goblin.hp - (slash + slash);
            print("Goblin Hp $doubleSl");
            goblin.hp = doubleSl;
            if (goblin.hp <= 0) {
              goblin.die();
              break;
            }
          }
        } else if (choicespecialplayer == "2") {
          print("--------------");
          print("     Miss     ");
          print("--------------");
        }
      } else if (randomside == "2") {
        print("Player select side attack");
        var choicespecialplayer = stdin.readLineSync()!;
        if (choicespecialplayer == "2") {
          print("⣶⣖⣒⣲⠶⠶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶" +
              "\n" +
              "⣿⣿⣿⣿⣿⣷⣦⣄⡙⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠻⣿⣿⣿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠹⢿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣆⢀⠈⢻⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⢀⢀⢻⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢀⢀⢀⢿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⢀⢀⢀⣸" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣭⣭⣀⣀⣀⣠⣴⣿" +
              "\n");
          print("Goblin get attacked by Player Slash $slash Damage");
          print("\n");
          var sl = goblin.hp - slash;
          print("Goblin Hp $sl");
          goblin.hp = sl;
          if (goblin.hp <= 40) {
            print("⣿⣿⣟⡻⠿⣿⣿⣿⣿⢿⣿⣿⣿⣿" +
                "\n" +
                "⣿⣿⣿⣿⣷⣬⡙⢿⡟⣼⣿⣿⣿⣿" +
                "\n" +
                "⣿⣿⣿⣿⣿⣿⣿⡆⡸⣿⣿⣿⣿⣿" +
                "\n" +
                "⣿⣿⣿⣿⣿⣿⢟⣾⣿⣹⣿⣿⣿⣿" +
                "\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
                "\n");
            print("Goblin get attacked by Player Double Slash $doubleslash Damage");
            var doubleSl = goblin.hp - (slash + slash);
            print("Goblin Hp $doubleSl");
            goblin.hp = doubleSl;
            if (goblin.hp <= 0) {
              goblin.die();
              break;
            }
          }
        } else if (choicespecialplayer == "1") {
          print("--------------");
          print("     Miss     ");
          print("--------------");
        }
      } else {
        print("⣶⣖⣒⣲⠶⠶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶" +
            "\n" +
            "⣿⣿⣿⣿⣿⣷⣦⣄⡙⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠻⣿⣿⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠹⢿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣆⢀⠈⢻⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⢀⢀⢻⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢀⢀⢀⢿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⢀⢀⢀⣸" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣭⣭⣀⣀⣀⣠⣴⣿" +
            "\n");
        print("Goblin get attacked by Player Slash $slash Damage");
        print("\n");
        var sl = goblin.hp - slash;
        print("Goblin Hp $sl");
        goblin.hp = sl;
        if (goblin.hp <= 40) {
          print("⣿⣿⣟⡻⠿⣿⣿⣿⣿⢿⣿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣷⣬⡙⢿⡟⣼⣿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⡆⡸⣿⣿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⢟⣾⣿⣹⣿⣿⣿⣿" +
              "\n" +
              "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
              "\n");
          print("Goblin get attacked by Player Double Slash $doubleslash Damage");
          var doubleSl = goblin.hp - (slash + slash);
          print("Goblin Hp $doubleSl");
          goblin.hp = doubleSl;
          if (goblin.hp <= 0) {
            goblin.die();
            break;
          }
        }
      }
    } else {
      print("--------------");
      print("     Miss     ");
      print("--------------");
    }
    goblinskillmenu();

    int scratch = goblin.skill() + goblin.atk;
    if (random == "1") {
      print("⢀⢀⢀⢀⢀⢀⢀⠠⣄⡀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⠈⠻⣶⡄⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⠈⠻⢦⡀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⠈⠂⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀⣤⡶⠞" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀⣤⣴⠶⠛⠉⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣀⣤⠶⠎⠋⠉⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⣀⣤⡶⠟⠋⠁⢀⢀⢀⢀⢀⢀⢀⡠⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⣠⡴⠖⠛⠉⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⡴⠊⢀⢀⢀⢀⢀" +
          "\n" +
          "⠠⠒⠉⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⡴⠋⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣠⡶⠋⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⣠⡾⠋⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⢀⢀⣠⡾⠊⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⢀⢀⣤⡾⠋⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⢀⢀⣴⡿⠋⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⢀⢀⣴⠟⠁⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n" +
          "⢀⡴⠋⠁⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀⢀" +
          "\n");
      print("Player get attacked by Goblin scratch $scratch Damage");
      print("\n");

      var sc = player.hp - scratch;
      print("Player Hp $sc");
      player.hp = sc;
      if (player.hp <= 0) {
        player.die();
        break;
      }
    } else {
      print("--------------");
      print("     Miss     ");
      print("--------------");
    }
  }
}

void objgoblin() {
  print("left press 1");
  print("right press 2");
}

void Trollbattle(Player player, Troll troll) {
  while (player.hp > 0 || troll.hp > 0) {
    const chars = "11223";

    String RandomString(int strlen) {
      Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
      String result = "";
      for (var i = 0; i < strlen; i++) {
        result += chars[rnd.nextInt(chars.length)];
      }
      return result;
    }

    var random = RandomString(1);
    playerskillmenu();
    var choice = stdin.readLineSync()!;
    int slash = player.skill()+player.atk;
    int doubleslash = slash+slash;
    if (choice == "1") {
      print("⣶⣖⣒⣲⠶⠶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶" +
          "\n" +
          "⣿⣿⣿⣿⣿⣷⣦⣄⡙⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠻⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠈⠹⢿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣆⢀⠈⢻⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⢀⢀⢻⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢀⢀⢀⢿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⢀⢀⢀⣸" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣭⣭⣀⣀⣀⣠⣴⣿" +
          "\n");
      print("Troll get attacked by Player Slash $slash Damage");
      print("\n");
      var sl = troll.hp - slash;
      print("Troll Hp $sl");
      troll.hp = sl;
      if (troll.hp <= 40) {
        print("Troll Hp has lower than 40 or equal 40");
        print("⣿⣿⣟⡻⠿⣿⣿⣿⣿⢿⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣷⣬⡙⢿⡟⣼⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⡆⡸⣿⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⢟⣾⣿⣹⣿⣿⣿⣿" +
            "\n" +
            "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
            "\n");
        print("Troll get attacked by Player Double Slash $doubleslash Damage");
        var doubleSl = troll.hp - (slash + slash);
        print("Troll Hp $doubleSl");
        troll.hp = doubleSl;
        if (troll.hp <= 0) {
          troll.die();
          break;
        }
      }
    } else {
      print("--------------");
      print("     Miss     ");
      print("--------------");
    }
    trollskillmenu();
    int smash = troll.skill()+troll.atk;
    int run = smash+troll.run();
    if (random == "1") {
      print("⣠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣄" +
          "\n" +
          "⣿⠻⠿⣿⡏⢔⠙⢿⣿⣿⡿⠛⣿⣿⡿⢋⢹⣿⣿" +
          "\n" +
          "⣿⡄⢺⣄⠑⣼⣷⡈⡛⣿⣶⢃⡛⣫⣶⡟⠇⣩⠛" +
          "\n" +
          "⣿⣷⡄⢻⣷⣿⣿⣧⣽⣿⣿⣮⣼⣿⣿⣧⣌⣀⠲" +
          "\n" +
          "⣿⡧⠍⣻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⡛⠱" +
          "\n" +
          "⣿⣷⣩⣿⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠿⣶⣵⠝" +
          "\n" +
          "⣃⣠⣤⣤⡆⡇⣿⡿⠋⣿⣿⣿⠇⢹⡇⢫⣄⣊⠅" +
          "\n" +
          "⣿⣿⣿⣿⡇⡇⠟⣰⣾⢸⠏⣫⣷⢊⢁⣿⣿⣿⣿" +
          "\n" +
          "⢿⣿⣿⣿⠸⣃⣼⣿⣏⣠⣾⣿⣿⣧⣸⣿⣿⣿⡿" +
          "\n" +
          "⢀⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛" +
          "\n");
      print("Player get attacked by Troll Smash $smash Damage");
      print("\n");

      var sm = player.hp - smash;
      print("Player Hp $sm");
      player.hp = sm;
      if (player.hp <= 0) {
        player.die();
        break;
      }
    } else if (random == "2") {
      print("⣿⣿⣿⣿⣿⣿⣿⣿⣓⠒⠉⠙⠛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣤⡀⢀⢀⠈⠛⢿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣷⣤⣄⠉⠙⠛⣿⣷⣤⡘⣷⣤⡘⢿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⣤⣽⢿⣿⣿⣿⣿⣦⢻⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⡿⠿⡿⣿⣿⣿⣛⣿⣿⣿⣿⣿⣿⣯⠻⠈⠘⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣷⡀⠈⢿⣶⣾⣶⣦⣉⣛⣿⡿⢿⣯⢀⢀⢀⢿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⢀⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣬⡁⢀⢀⢸" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⡅⢀⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⢀⢀⢸" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⡀⠈⢿⣿⣿⣿⣿⣿⣿⣿⡿⢟⣫⠄⢀⢀⢸" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⡟⠁⡠⣿⣛⣻⣟⣉⣤⣴⣿⣿⣿⣯⢀⢀⢀⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣯⣼⢦⢰⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠛⢉⣽⣿⡿⣿⣿⡿⢋⣾⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣛⣛⣁⣤⣴⣶⣿⠟⠋⣰⠟⢋⣠⣾⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠋⢀⢀⢀⣠⣴⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n" +
          "⣿⣿⣿⣿⣿⣿⣿⣭⣴⣒⣤⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿" +
          "\n");
      print("Troll Run to Player and Charge to Player $run Damage");
      print("\n");
      var ro = player.hp - run;
      print("Player Hp $ro");
      player.hp = ro;
      if (player.hp <= 0) {
        player.die();
        break;
      }
    } else {
      print("--------------");
      print("     Miss     ");
      print("--------------");
    }
  }
}

void opponentlist() {
  print("|------------------------|");
  print("|  Choose your opponent  |");
  print("|     Goblin press 1     |");
  print("|     Spider press 2     |");
  print("|     Troll press 3      |");
  print("|     Dragon press 4     |");
  print("|------------------------|");
}

void trollskillmenu() {
  print("|------------------------|");
  print("|       Troll TURN       |");
  print("|------------------------|");
}

void playerskillmenu() {
  print("|------------------------|");
  print("|      Player TURN       |");
  print("|  Player slash press 1  |");
  print("|------------------------|");
}

void goblinskillmenu() {
  print("|------------------------|");
  print("|      Goblin Turn       |");
  print("|------------------------|");
}

void spiderskillmenu() {
  print("|------------------------|");
  print("|      Spider Turn       |");
  print("|------------------------|");
}

void dragonskillmenu() {
  print("|------------------------|");
  print("|      Dragon Turn       |");
  print("|------------------------|");
}
